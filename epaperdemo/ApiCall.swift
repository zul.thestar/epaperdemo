//
//  ApiCall.swift
//  epaperdemo
//
//  Created by Mohd Zulhilmi Bin Mohd Zain on 20/06/2023.
//

import Foundation
import Alamofire

class ApiCall {

    func callForEpaper(completion: @escaping (_ result: Epapers?, _ responseCode: Int?)->Void) {

        AF.request("https://clips.thestar.com.my/TSM/starbiz7/example.json", method: HTTPMethod.get) { request in
            request.timeoutInterval = 20.0
            request.allowsConstrainedNetworkAccess = true
            request.allowsCellularAccess = true
            request.allowsExpensiveNetworkAccess = true
        }.response { responseData in

            do {
                let extracted = try JSONDecoder.init().decode(Epapers.self, from: responseData.data!)
                completion(extracted, responseData.response?.statusCode)
            } catch {
                completion(nil, responseData.response?.statusCode)
            }

        }

    }
}
