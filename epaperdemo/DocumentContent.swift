//
//  PDFReader.swift
//  epaperdemo
//
//  Created by Mohd Zulhilmi Bin Mohd Zain on 21/06/2023.
//

import SwiftUI
import PDFKit
import Alamofire

struct DocumentContent: View {
    @State var fullDoc: String!
    @State var docsData: [Document]!

    var body: some View {
        GeometryReader.init { proxy in
            ScrollView.init {
                LazyVStack.init(alignment: HorizontalAlignment.center) {
                    //PDFReader.init(docUrl: URL.init(string: self.fullDoc ?? "")).frame(maxWidth: .infinity, minHeight: proxy.size.height)
                    ForEach(0 ..< (self.docsData.count), id: \.self) { count in
                        let reader = PDFReader.init(docUrl: URL.init(string: self.docsData[count].pdf ?? ""))
                        reader.onAppear {
                            debugPrint("Appearing...")
                            debugPrint("IsFinishLoad: \(reader.readerObservable.pdfView)")
                        }
                    }.id(self.docsData.count)
                        .frame(maxWidth: .infinity, minHeight: proxy.size.height)
                }.frame(maxWidth: .infinity)
            }.frame(maxWidth: .infinity, minHeight: proxy.size.height)
        }.edgesIgnoringSafeArea([.top,.bottom])
    }
}

struct PDFReader: UIViewRepresentable {

    @State var docUrl: URL!
    @State private var loadingIndicator: UIActivityIndicatorView!
    @ObservedObject var readerObservable: PDFViewerObservable = PDFViewerObservable.init()

    func makeUIView(context: Context) -> PDFView {
        let pdfViewer: PDFView = PDFView.init()
        pdfViewer.displayMode = PDFDisplayMode.singlePage
        pdfViewer.interpolationQuality = PDFInterpolationQuality.low
        pdfViewer.displayDirection = PDFDisplayDirection.vertical
        self.readerObservable.pdfView = pdfViewer

        let loadingIndicator = UIActivityIndicatorView.init()
        loadingIndicator.tag = 1
        loadingIndicator.translatesAutoresizingMaskIntoConstraints = false
        loadingIndicator.style = UIActivityIndicatorView.Style.large
        loadingIndicator.startAnimating()
        pdfViewer.addSubview(loadingIndicator)

        loadingIndicator.centerXAnchor.constraint(equalTo: pdfViewer.centerXAnchor).isActive = true
        loadingIndicator.centerYAnchor.constraint(equalTo: pdfViewer.centerYAnchor).isActive = true

        return pdfViewer
    }

    func updateUIView(_ pdfViewer: PDFView, context: Context) {
        debugPrint("UpdatePDFView")
        //pdfViewer.document = PDFDocument.init(url: self.docUrl)\

        let indicator: UIActivityIndicatorView? = pdfViewer.viewWithTag(1) as? UIActivityIndicatorView

        self.loadPDFData(url: self.docUrl) { pdfData, error in

            indicator?.stopAnimating()

            if(error == nil) {
                debugPrint("PDF Loaded")
                pdfViewer.document = PDFDocument.init(data: pdfData!)
                pdfViewer.maxScaleFactor = 4.0
                pdfViewer.minScaleFactor = pdfViewer.scaleFactorForSizeToFit
                pdfViewer.autoScales = true
            } else {
                debugPrint("Error when loading PDF: \(error.debugDescription)")
            }
        }
    }

    func loadPDFData(url: URL, completion: @escaping ((_ pdfData: Data?, _ error: Error?)->Void)) {
        if url.isFileURL == false {
            debugPrint("Loading?")
        }

        AF.request(url) { request in
            request.timeoutInterval = 20.0
            request.allowsCellularAccess = true
            request.allowsExpensiveNetworkAccess = true
            request.allowsConstrainedNetworkAccess = true
        }.response { returned in
            switch returned.result {
            case .success(let data):
                completion(data, nil)
            case .failure(let err):
                completion(nil, err)
            }
        }
    }
}

class PDFViewerObservable: ObservableObject {

    private var _objectFinish: Bool = false
    var objectFinish: Bool {
        get {
            return _objectFinish
        } set (newVal) {
            _objectFinish = newVal
        }
    }

    private var _pdfView: PDFView!
    var pdfView: PDFView {
        get {
            return _pdfView
        } set (newVal) {
            _pdfView = newVal
        }
    }
}
