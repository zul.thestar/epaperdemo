//
//  Epapers.swift
//  epaperdemo
//
//  Created by Mohd Zulhilmi Bin Mohd Zain on 20/06/2023.
//

import Foundation

// MARK: - JSON
struct Epapers: Codable {
    var epapers: [Epaper]?
}

// MARK: - Epaper
struct Epaper: Codable {
    var id: Int?
    var title, description: String?
    var thumbnail: String?
    var date: String?
    var fullDocument: String?
    var documents: [Document]?

    enum CodingKeys: String, CodingKey {
        case id, title, description, thumbnail, date, documents
        case fullDocument = "full_document"
    }
}

// MARK: - Document
struct Document: Codable {
    var pdf: String?
    var thumbnail: String?
    var pageIndex: Int?
}


