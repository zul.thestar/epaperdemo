//
//  ContentView.swift
//  epaperdemo
//
//  Created by Mohd Zulhilmi Bin Mohd Zain on 16/06/2023.
//

import SwiftUI
import Kingfisher

//Epaper Card
struct EpaperMagazine: View {
    @State var title: String!
    @State var description: String!
    @State var imageUrl: String!
    @State var geometryProxy: GeometryProxy!
    @State var itemPerRow: CGFloat! = 2
    @State var height: CGFloat! = 250
    @State var horizontalSpacing: CGFloat! = 15

    var body: some View {
        VStack.init() {
            KFImage.init(URL.init(string: self.imageUrl)).resizing(referenceSize: CGSize.init(width: self.getWidth(geometry: geometryProxy), height: self.height)).shadow(color: Color.gray, radius: 7.0)
            LazyVStack.init {
                Text.init(self.title).frame(maxWidth: CGFloat.infinity, alignment: Alignment.leading)
                Text.init(self.description).frame(maxWidth: CGFloat.infinity, alignment: Alignment.leading).font(Font.system(size: 12.0)).foregroundColor(Color.gray)
            }.frame(maxWidth: CGFloat.infinity, alignment: Alignment.leading)
        }
        .padding(EdgeInsets.init(top: 5.0, leading: 5.0, bottom: 5.0, trailing: 5.0))
    }

    func getWidth(geometry: GeometryProxy) -> CGFloat {
        let preWidth = (geometry.size.width - self.horizontalSpacing * (self.itemPerRow + 1))
        let width: CGFloat = preWidth / self.itemPerRow
        return width
    }
}

struct ContentView: View {

    @State private var showAlert: Bool = false
    @State private var epapersData: Epapers? = Epapers.init()

    var body: some View {
        NavigationView.init() {
            GeometryReader.init() { geometry in
                ScrollView.init() {
                    LazyVGrid.init(columns: [GridItem.init(.flexible()),GridItem.init(.flexible())]) {
                        ForEach.init(0 ..< (self.epapersData?.epapers?.count ?? 0), id: \.self) { value in
                            NavigationLink.init(destination: { DocumentContent.init(fullDoc: self.epapersData?.epapers?[value].fullDocument, docsData: self.epapersData?.epapers?[value].documents) }) {
                                EpaperMagazine.init(title: self.epapersData?.epapers?[value].title, description: self.epapersData?.epapers?[value].description, imageUrl: self.epapersData?.epapers?[value].thumbnail, geometryProxy: geometry)
                            }
                        }.id(self.epapersData?.epapers?.count)
                    }
                }
                .padding()
                .onAppear {
                    Task.init() {
                        await self.callApi()
                    }
                }
            }
            .navigationTitle("Starbiz7")
            .refreshable {
                await self.callApi()
            }
        }
    }

    func callApi() async {
        debugPrint("Calling api...")
        let apiCall = ApiCall.init()
        apiCall.callForEpaper { result, respCode in
            self.epapersData = result
        }
    }
}

/*
struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        EpaperCard.init(title: "test")
    }
}
*/
