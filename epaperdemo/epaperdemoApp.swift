//
//  epaperdemoApp.swift
//  epaperdemo
//
//  Created by Mohd Zulhilmi Bin Mohd Zain on 16/06/2023.
//

import SwiftUI

@main
struct epaperdemoApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView.init()
            //EpaperMagazine.init(title: "Image", description: "Desc 1", imageUrl: "https://clips.thestar.com.my/TSM/starbiz7/20221027/1.jpg")
        }
    }
}
